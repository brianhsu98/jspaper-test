/**
 * Constructs a flag consisting of rectangles stacked on top of another.
 * @param topLeft: The top left point of the flag
 * @param rectSize: The individual size of each of the rectangles
 * @param colors: The colors of the flag, in the desired order.
 * Returns a group containing all the rectangles of the flag.
*/ 
function constructFlag(topLeft, rectSize, colors) {
	var topLeft = topLeft;
	var rectSize = rectSize;
	var rect;
	var path;
	var group = new Group();
	for (i = 0; i < colors.length; i += 1) {
		rect = new Rectangle(topLeft, rectSize);
		topLeft = rect.bottomLeft;
		path = new Path.Rectangle(rect);
		path.fillColor = colors[i]
		group.addChild(path);
	}
	return group;
}


pride = ['red', 'orange', 'yellow', 'green', 'blue', 'purple'];

var pride = constructFlag(new Point(0, 0), new Size(view.size.width, view.size.height / pride.length), pride, false);

small = false;

function onMouseDown(event) {
	if (!small) {
		small = true;
		pride.scale(.5);
	}
	pride.position = event.point;
}

function onMouseDrag(event) {
	pride.position = event.point;
}