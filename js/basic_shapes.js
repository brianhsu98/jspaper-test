var circle = new Path.Circle({
    center: view.center,
    radius: 60,
    strokeColor: 'red'
});

var rectangle = new Path.Rectangle({
    center: new Point(20, 150),
    size: new Size(50, 50),
    fillColor: 'blue'
});

var line = new Path.Line(new Point(20, 200), view.center);
line.strokeColor = 'black';

var from = new Point(170, 120);
var through = new Point(200, 180);
var to = new Point(170, 220);
var curvedPath = new Path.Arc(from, through, to);
curvedPath.strokeColor = 'black';

var point = new Point(20, 50);
var size = new Size(100, 50);
var myRectangle = new Path.Rectangle(point, size);
myRectangle.fillColor = 'lightBlue';

var myBall = new Path.Circle(new Point(10, 90), 10);
myBall.fillColor = 'black';



rectangleSymbol = new Symbol(rectangle);
for (var i = 0; i < 5; i++) {
    var placedSymbol =rectangleSymbol.place(new Point(20 + (i * 40), 50));
    placedSymbol.rotate(i * 10); // operation on the instance
}


function onFrame(event) {
    // Normalise the event.count property to a 0-359 scale
    // then apply some trigonometry so we get some smoothed values
    // just like going round the edge of a ball
    var x = (1 + Math.cos((event.count * 2 % 360)
        * (Math.PI / 180))) * 100 + 10
    var y = (Math.abs(Math.sin((event.count * 2 % 360)
        * (Math.PI / 180)))) * 80;
    myRectangle.position.x = x;
    myBall.position.x = x;
    myBall.position.y = 90 - y;
    rectangleSymbol.definition.fillColor.hue += 1;
    // rotate
    rectangleSymbol.definition.rotate(0.2);
}