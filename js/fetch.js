var ball;
var dog;
var dest;
initializePath();

function initializePath() {
    ball = new Path.Circle(view.center, 30);
    ball.fillColor = {
        hue: Math.random() * 360,
        saturation: 1,
        brightness: 1
    };
    dog = new Raster('dog');
    dog.position = view.center;
    dog.scale(0.25);
    dest = view.center;
}

function onFrame(event) {
    var dogVector = ball.position - dog.position;
    if (dogVector.length > 3) {
        dog.position += dogVector / 30;
    }
    var ballVector = dest - ball.position;
    ball.position += ballVector / 20;
}

function onMouseDown(event) {
    dest = Point.random() * new Point(view.size);
}
