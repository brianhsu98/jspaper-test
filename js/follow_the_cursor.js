var circle;
var dog;
var mousePos = view.center / 2;
initializePath();

function initializePath() {
    circle = new Path.Circle(mousePos, 30);
    circle.fillColor = {
        hue: Math.random() * 360,
        saturation: 1,
        brightness: 1
    };
    dog = new Raster('dog');
    dog.position = mousePos;
    dog.scale(.25);
}

function onFrame(event) {
    circle.position = mousePos;
    var vector = mousePos - dog.position;
    if (vector.length > 3) {
        dog.position += vector / 30;
    }
}

function onMouseMove(event) {
    mousePos = event.point;
}
