var boing = new PointText({
    point: view.center,
    justification: 'center',
    fontSize: 30,
    fillColor: 'white',
    content: 'boing'
});

function getNewDestination() {
    var top = Point.random() * new Point(view.size.width, 0);
    var bottom = top.clone();
    bottom.y = view.size.height;
    var left = Point.random() * new Point(0, view.size.height);
    var right = left.clone();
    right.x = view.size.width;
    choices = [top, bottom, left, right];
    return choices[Math.floor(Math.random()*choices.length)];
}

dest = getNewDestination();
console.log(dest);

function onFrame(event) {
    var vector = dest - boing.position;

    boing.position += vector/30;

    if (vector.length < 5) {
        dest = getNewDestination()
    }

}
